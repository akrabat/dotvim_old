" NOTE: http://superuser.com/questions/319591/how-can-i-prevent-macvim-from-showing-os-x-find-replace-dialog-when-pressing-co


" no beeps
set visualbell
set t_vb=
set guitablabel=%M\ %t

set cursorline                  " highlight the line that the cursor is on

" No toolbars, menu or scrollbars in the GUI
set clipboard+=unnamed
set vb t_vb=
set guioptions-=m  "no menu
set guioptions-=T  "no toolbar
set guioptions-=l  "no left hand scrollbar
set guioptions-=L  "no left hand scrollbar when split
set guioptions+=r  "right hand scrollbar
set guioptions+=R  "right hand scrollbar when split


" MacVim modifications
if has("gui_macvim")
  " Disable print shortcut for 'goto anything...'
  " macmenu File.Print key=<nop>

  " Disable close shortcut so we can use it for :bd
  macmenu File.Close key=<nop>
  map <D-w> :bd<cr>
  imap <D-w> <esc>:bd<cr>

  " Bind new tab to cmd+alt+shift+n
  macmenu File.New\ Tab key=<D-M-N>
    
  " Move with shift+cmd+left/right
  macm Window.Select\ Previous\ Tab  key=<D-M-LEFT>
  macm Window.Select\ Next\ Tab    key=<D-M-RIGHT>

  " Open new window via cmd+shift+n
  macmenu File.New\ Window key=<D-N>

  " Shift arrows to select
  let macvim_hig_shift_movement = 1

  " Automatically resize splits when resizing MacVim window
  autocmd VimResized * wincmd =

  " "http://stackoverflow.com/questions/14802689/macvim-wont-load-specific-color-scheme-by-default
  " highlight SignColumn guibg=#272822
  set guifont=Menlo:h12
  colorscheme rka2

  " Open goto file
  map <D-t> :CtrlP<cr>
  map <D-R> :CtrlPTag<CR>
  "map <D-t> :CommandT<CR>
  "map <D-R> :CommandTTag<CR>

  macmenu &File.Save key=<nop>
  imap <D-s> <esc>:w<cr><esc>
  map <D-s> <esc>:w<cr><esc>

  " Indent lines with cmd+[ and cmd+]
  " nmap <D-]> >>
  " nmap <D-[> <<
  " vmap <D-[> <gv
  " vmap <D-]> >gv

  "Open sidebar with shift+cmd+1
  map <S-D-1> :NERDTreeTabsToggle<CR>

  " This mapping makes Ctrl-Tab switch between tabs.
  " Ctrl-Shift-Tab goes the other way.
  "noremap <C-Tab> :tabnext<CR>
  "noremap <C-S-Tab> :tabprev<CR>
  nnoremap <C-Tab> :bn<CR>
  nnoremap <C-S-Tab> :bp<CR>
  inoremap <esc><C-Tab> :bn<CR>
  inoremap <esc><C-S-Tab> :bp<CR>




  " Select text whit shift
  let macvim_hig_shift_movement = 1

  " Stop completion with enter, in addition to default ctrl+y
  "imap <expr> <CR> pumvisible() ? "\<c-y>" : "<Plug>delimitMateCR"


  " set macmeta
  " inomap <silent> <M-3> # 
  " inomap <silent> <M-2> €

endif
