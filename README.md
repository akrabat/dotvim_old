#Dot VIM

* Place this folder in ~/.vim
* Symlink vimrc to ~/.vimrc: ln -s ~/.vim/vimrc ~/.vimrc
* Symlink gvimrc to ~/.gvimrc: ln -s ~/.vim/gvimrc ~/.gvimrc
* mkdir ~/.vim.backup

