" PHP specific settings


setlocal eol

" Cliff's tab settings
set nosmarttab                        " Insert tabs on the start of a line according to tabstop
set shiftwidth=4                      " Use indents of 4 spaces
set noexpandtab                       " Tabs are tabs, not spaces
set tabstop=8                         " Tabs are 8 spaces though
set softtabstop=4                     " Let backspace delete spaced indent


let perl_include_pod   = 1    "include pod.vim syntax file with perl.vim"
let perl_extended_vars = 1    "highlight complex expressions such as @{[$x, $y]}"
let perl_sync_dist     = 250  "use more context for highlighting"


" autocmd FileType pl set omnifunc=perlcomplete#OmniPerl_Complete
" :au! CursorHold * nested call PreviewWord()


" function! SmartComplete ()
" " Remember where we parked...
" let cursorpos = getpos('.')
" let cursorcol = cursorpos[2]
" let curr_line = getline('.')
"
" " Special subpattern to match only at cursor position...
" let curr_pos_pat = '\%' . cursorcol . 'c'
"
" " Tab as usual at the left margin...
" if curr_line =~ '^\s*' . curr_pos_pat
" return "\<TAB>"
" endif
"
" " If no contextual match and after an identifier, do keyword completion...
" if curr_line =~ '\k' . curr_pos_pat
" return "\<C-N>"
"
" " Otherwise, just be a ...
" endif
" if curr_line =~ '\(\->\|::\)' . curr_pos_pat
" return "\<C-X>\<C-O>"
" else
" return "\<TAB>"
" endif
" endfunction
"
" " Remap for smart completion on various characters...
" "inoremap <tab> :call SmartComplete()<CR>
