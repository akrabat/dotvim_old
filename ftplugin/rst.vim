" RST specific settings

nnoremap <F5> yyp<c-v>$r=
inoremap <F5> <Esc>yyp<c-v>$r=A

function! s:Underline(chars)
  let chars = empty(a:chars) ? '-' : a:chars
  let nr_columns = virtcol('$') - 1
  let uline = repeat(chars, (nr_columns / len(chars)) + 1)
  put =strpart(uline, 0, nr_columns)
endfunction
command! -nargs=? Underline call s:Underline(<q-args>)

nnoremap <F6> :w<CR> :!time create.sh<CR>
inoremap <F6> <ESC>:w<CR> :!time create.sh<CR>

nmap <buffer> <leader>em ysiw*


