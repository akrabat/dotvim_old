" PHP specific settings

setlocal eol
set smarttab                          " Insert tabs on the start of a line according to shiftwidth, not tabstop
set shiftwidth=4                      " Use indents of 4 spaces
set expandtab                         " Tabs are spaces, not tabs
set tabstop=4                         " An indentation every four columns
set softtabstop=4                     " Let backspace delete indent
