" Markdown specific settings

setlocal nosmartindent
setlocal spell spelllang=en_gb

nmap <buffer> <leader>em ysiw*

" :nnoremap <leader>b :silent !open -a Marked\ 2.app '%:p'<cr>

set makeprg=open\ -a\ Marked\\\ 2.app\ '%:p'

