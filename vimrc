" See also: http://vim.wikia.com/wiki/Keep_your_vimrc_file_clean

set nocompatible                    " Use ViMproved, don't emulate old vi
let $VIMHOME = split(&rtp, ',')[0]  " Find the Vim path

" =========================================================================
" Pathogen
" =========================================================================
execute pathogen#infect()



" =========================================================================
" Settings
" =========================================================================

filetype plugin indent on               " Automatically detect file types.
let mapleader = ","                     " Set mapleader to ","


set omnifunc=syntaxcomplete#Complete    " Omni completion

set t_Co=256
set autowrite                           " Automatically save before :next, :make etc.
set backupcopy=yes                      " Preserve Finder labels on OS X
set hidden                              " Buffer switching without saving
set hlsearch                            " Highlight search terms
set ignorecase                          " case insensitive search
set mouse=a                             " Enable mouse usage
set mousemodel=popup                    " Enable context menu
set number                              " Display line numbers
set shiftround                          " Use multiple of shiftwidth when indenting with '<' and '>'
set shortmess+=I                        " Disable splash text
set showmatch                           " Show matching brackets/parenthesis
set smartcase                           " Case sensitive when an upper case letter is present
set timeoutlen=500                      " Timeout for mappings
set title                               " Change the terminal's title
set undolevels=1000                     " Many levels of undo
set nofoldenable                        " Turn off code folding
set noerrorbells                        " No beeps
set novisualbell                        " No flashes
set t_vb=                               " No flashes
set backupdir=~/.vim.backup             " Directory for backup files when saving
set directory=~/.vim.backup             " Directory for swap files
set splitright                          " Split vertical windows right to the current windows
set splitbelow                          " Split horizontal windows below to the current windows
set whichwrap+=<,>,h,l,[,]              " allow cursor to move to next line
set linebreak                           " wrap long lines at a character in 'breakat'

"set virtualedit=all                     " allow for cursor beyond last character

set wildmode=list:longest,full          " <Tab> completion: list matches, then longest common part
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pdf,*.phar,*.class,*.pyc,*.bak

" tab settings
set shiftwidth=4                        " Use indents of 4 spaces
set expandtab                           " Tabs are spaces, not tabs
set tabstop=4                           " An indentation every four columns
set softtabstop=4                       " Let backspace delete indent

  
" List characters
set list



" Function to tell us the highlight group syntax
function! SyntaxItem()
    let l:name = synIDattr(synID(line("."),col("."),1),"name")
    if l:name == ''
        return ''
    endif
    return '[' . l:name . ']'
endfunction

" Status line

" default the statusline to green when entering Vim
hi statusline guibg=DarkGreen ctermfg=8 guifg=White ctermbg=15
" hi statuslineNC guibg=Black ctermfg=8 guifg=White ctermbg=15 gui=italic

" Formats the statusline
set statusline=%f                           " file name
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag

" Puts in the current git status
set statusline+=%{fugitive#statusline()}

" Puts in syntastic warnings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set statusline+=\ %{SyntaxItem()}


set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L                  " line X of Y
set statusline+=\ Col:%c                    " current column
set statusline+=\ Buf:%n                    " Buffer number
set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor


" =========================================================================
" Key mappings
" =========================================================================

set pastetoggle=<F2>

" navigate virtual lines
nnoremap j gj
nnoremap k gk

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" ,o to replace current word and set up so that . will do the same on the next
" instance of the current word
nmap <leader>o *Ncgn

" Underline the current line with '='
nmap <silent> <leader>ul :t.<CR>Vr=

" For when you forget to sudo.. Really Write the file.
cmap w!! w !sudo tee % >/dev/null

" control-tab to switch buffers
"noremap <C-TAB>   :bnext<CR>
"noremap <C-S-TAB> :bprev<CR>

" clear matches using leader-space
nmap <leader><space> :noh<cr>


"Shortcut to auto indent entire file
"nmap <S-C-r> 1G=G``
"imap <S-C-r> <ESC>1G=Ga``

" Remove the Windows ^M - when the encodings gets messed up
nmap <Leader>M mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

"preview ctag
nmap <D-]> :exe "ptjump " . expand("<cword>")<Esc>
imap <D-]> :exe "ptjump " . expand("<cword>")<Esc>

" Toggle wordwrap on leader-w
map <leader>w :set nowrap!<CR>

" Close Quickfix window (,qq)
map <leader>qq :cclose<CR>

" Navigate through the Quickfix results with + and -
nnoremap +      :cnext<CR> :norm! zz<CR> :TlistSync<CR>
nnoremap _      :cprev<CR> :norm! zz<CR> :TlistSync<CR>


"Omni complete on ctrl-space
inoremap <C-Space> <C-x><C-o>
inoremap <C-@> <C-Space>

" Close every buffer
map <leader>qa :1,1000 bd!<cr>

" Ctrl+h/k for buffer switching
nnoremap <C-h> :bp<CR>
nnoremap <C-l> :bn<CR>

" put the caret in a logical place after a visual mode yank
vmap y ygv<Esc>

" Switch between last two buffers
nnoremap <leader><leader> <C-^>

"What syntax highight is under the cursor?
map <F9> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>


" shift+P should always paste from yank buffer
nnoremap <leader>p "0p

" Easier copy/paste
    " map <leader>v "+gP
    " map <leader>c "+y

" Ctrl-a for select all
map <C-A> ggVG

" Yankstack keys
if exists("g:yankstack_size")
  call yankstack#setup()
  nmap <leader>p <Plug>yankstack_substitute_older_paste
  nmap <leader>P <Plug>yankstack_substitute_newer_paste
  nmap <c-p> <Plug>yankstack_substitute_older_paste
  nmap <c-s-p> <Plug>yankstack_substitute_newer_paste
endif

" map Y to work like D and C
nmap Y y$

" CtrlP keys
" let g:ctrlp_max_files=0
" nmap <leader>t :CtrlP<cr>
" nmap <leader>b :CtrlPBuffer<cr>
" nmap <leader>k :CtrlPTag<cr>
" nmap <leader>j :CtrlPBufTag<cr>


" Command-T
" default: nmap <leader>t :CommandT<cr>
" default: nmap <leader>b :CommandTBuffer<cr>
nmap <leader>k :CommandTTag<cr>
nnoremap <silent> <leader>b :CommandTMRU<CR>

" TagList keys
nnoremap <silent> <F8> :TlistToggle<CR>
nnoremap <silent> <D-r> :TlistToggle<CR>
nnoremap <silent> <leader>r :TlistToggle<CR>

" recover from accidental C-u - http://vim.wikia.com/wiki/Recover_from_accidental_Ctrl-U
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
inoremap <C-U> <C-G>u<C-U>

" Ctrl+A to go to start of command line
cnoremap <C-A> <Home>

nmap <D-0> :tnext<CR>
nmap <D-9> :tnext<CR>

" scroll other split
nmap <D-j> <c-w>w<c-e><c-w>w
nmap <D-k> <c-w>w<c-y><c-w>w


" =========================================================================
" Plugin configuration
" =========================================================================

" Close the Omni-Completion tip window when leaving insert mode
augroup close_ocwindow
    autocmd!
    autocmd InsertLeave * if pumvisible() == 0|pclose|endif
augroup END


" Open in browser
nmap gb <Plug>(openbrowser-smart-search) 
vmap gb <Plug>(openbrowser-smart-search) 

nmap gh :OpenGithubFile<CR>
vmap gh :OpenGithubFile<CR>

" Dash
:nmap <silent> <leader>h <Plug>DashSearch

" Grep
let g:Grep_Default_Options = "-iHnR"
let Grep_Skip_Dirs = '.svn .git tmp var _var'
let Grep_Skip_Files = '*~ *.pdf *.phar *.log'

" vim-gist
let g:gist_post_private = 1
let g:gist_show_privates = 1
let g:gist_detect_filetype = 1
let g:gist_open_browser_after_post = 1
let g:gist_clip_command = 'pbcopy'

"pastebin
let g:pastebin_api_dev_key="4d2e29b03694cb5905be55418acf17d5"
let g:pastebin_api_user_name="akrabat"
let g:pastebin_api_user_password="3JuFvmRBf8sa62HWxh4w"
let g:pastebin_expire_date="N"
let g:pastebin_private="1"

" netrw
"let g:netrw_liststyle=3
"let g:netrw_browse_split=4      " Open file in previous buffer

" pdv-standalone
" use "" as parameter to turn tag off
let g:pdv_cfg_php4guess=0
let g:pdv_cfg_Package=" "
let g:pdv_cfg_Author=" "
let g:pdv_cfg_Version=" "
"let g:pdv_cfg_Copyright=""
"let g:pdv_cfg_License=""
nnoremap <C-K> :call PhpDocSingle()<CR>
vnoremap <C-K> :call PhpDocRange()<CR>

" set the names of flags
let tlist_php_settings = 'php;c:class;f:function;d:constant'
" close all folds except for current file
let Tlist_File_Fold_Auto_Close = 1
" make tlist pane active when opened
let Tlist_GainFocus_On_ToggleOpen = 1
" width of window
let Tlist_WinWidth = 40
" close tlist when a selection is made
let Tlist_Close_On_Select = 1
" one file at a time
let Tlist_Show_One_File = 1


" Syntastic
" On check for php syntax errors. default is ['php' , 'phpcs', 'phpmd']
let g:syntastic_php_checkers=['php', 'phpcs']
let g:syntastic_html_tidy_ignore_errors=['<link> escaping malformed URI reference']
let g:syntastic_php_phpcs_args='--standard=PSR2 -n'
"let g:syntastic_auto_loc_list=1
let g:syntastic_perl_checkers = ['podchecker']
let g:syntastic_enable_swift_xcrun_checker=1

" CtrlP
let g:ctrlp_extensions = ['tag', 'buffertag']
"let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:10,results:50'
let g:ctrlp_match_window = 'bottom,order:ttb,min:1,max:20,results:50'
" Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
" ag is fast enough that CtrlP doesn't need to cache
let g:ctrlp_use_caching = 0


" Ag
" Use ag over grep
" set grepprg=ag\ --nogroup\ --nocolor



" UltiSnips
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<tab>"
"let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
inoremap <c-x><c-k> <c-x><c-k>

" Command-T
let g:CommandTTagIncludeFilenames=1
let g:CommandTMaxHeight = 20
"let g:CommandTMatchWindowReverse = 1

" NERDTree
map <leader>l :NERDTreeFind<CR>
map <leader>e :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1

" Riv (rst)
let g:riv_disable_folding = 1

" pdv
let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
nnoremap <buffer> <C-p> :call pdv#DocumentWithSnip()<CR>

" =========================================================================
" Functions
" =========================================================================

" http://nvie.com/posts/how-i-boosted-my-vim/
fun! s:ToggleMouse()
    if !exists("s:old_mouse")
        let s:old_mouse = "a"
    endif

    if &mouse == ""
        let &mouse = s:old_mouse
        echo "Mouse is for Vim (" . &mouse . ")"
    else
        let s:old_mouse = &mouse
        let &mouse=""
        echo "Mouse is for terminal"
    endif
endfunction

" Source: http://vim.wikia.com/wiki/VimTip165
" Cleanly deletes a buffer without messing up the window layout.
" Modified by Evan Coury to prompt for unsaved changes.
function! s:Kwbd(kwbdStage)
  if(a:kwbdStage == 1)
    if(!buflisted(winbufnr(0)))
      bd!
      return
    endif
    let s:kwbdBufNum = bufnr("%")
    let s:kwbdWinNum = winnr()
    windo call s:Kwbd(2)
    execute s:kwbdWinNum . 'wincmd w'
    let s:buflistedLeft = 0
    let s:bufFinalJump = 0
    let l:nBufs = bufnr("$")
    let l:i = 1
    while(l:i <= l:nBufs)
      if(l:i != s:kwbdBufNum)
        if(buflisted(l:i))
          let s:buflistedLeft = s:buflistedLeft + 1
        else
          if(bufexists(l:i) && !strlen(bufname(l:i)) && !s:bufFinalJump)
            let s:bufFinalJump = l:i
          endif
        endif
      endif
      let l:i = l:i + 1
    endwhile
    if(!s:buflistedLeft)
      if(s:bufFinalJump)
        windo if(buflisted(winbufnr(0))) | execute "b! " . s:bufFinalJump | endif
      else
        enew
        let l:newBuf = bufnr("%")
        windo if(buflisted(winbufnr(0))) | execute "b! " . l:newBuf | endif
      endif
      execute s:kwbdWinNum . 'wincmd w'
    endif
    if(buflisted(s:kwbdBufNum) || s:kwbdBufNum == bufnr("%"))
      execute ":confirm :bd " . s:kwbdBufNum
    endif
    if(!s:buflistedLeft)
      set buflisted
      set bufhidden=delete
      set buftype=
      setlocal noswapfile
    endif
  else
    if(bufnr("%") == s:kwbdBufNum)
      let prevbufvar = bufnr("#")
      if(prevbufvar > 0 && buflisted(prevbufvar) && prevbufvar != s:kwbdBufNum)
        b #
      else
        bn
      endif
    endif
  endif
endfunction

command! Kwbd call s:Kwbd(1)
nnoremap <silent> <Plug>Kwbd :<C-u>Kwbd<CR>
nmap <silent> <leader>q :Kwbd<CR>

" Close current buffer and preserve window split
nmap <silent> <leader>d :b#<bar>bd#<bar>b<CR>

"----------------------------------------------------------------------------
" Move lines up / down : ctrl+shift+up/down
" See: http://stackoverflow.com/questions/741814/move-entire-line-up-and-down-in-vim
function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

noremap <silent> <c-s-up> :call <SID>swap_up()<CR>
noremap <silent> <c-s-down> :call <SID>swap_down()<CR>

"----------------------------------------------------------------------------
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set tabstop=%d softtabstop=%d shiftwidth=%d %sexpandtab :",
        \ &tabstop, &softtabstop, &shiftwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), '')
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

function! AppendCliffModeline()
  let l:modeline = " vim: set tabstop=8 softtabstop=4 shiftwidth=4 noexpandtab :"
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), '')
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>cml :call AppendCliffModeline()<CR>


"----------------------------------------------------------------------------
" Highlight matches to the current word without moving
:nnoremap <F7> :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>
"set guioptions+=a  " Automatically copy text on selection
set guioptions-=T   " Turn off toolbar
function! MakePattern(text)
    let pat = escape(a:text, '\')
    let pat = substitute(pat, '\_s\+$', '\\s\\*', '')
    let pat = substitute(pat, '^\_s\+', '\\s\\*', '')
    let pat = substitute(pat, '\_s\+',  '\\_s\\+', 'g')
    return '\\V' . escape(pat, '\"')
endfunction
vnoremap <silent> <F7> :<C-U>let @/="<C-R>=MakePattern(@*)<CR>"<CR>:set hls<CR>

"----------------------------------------------------------------------------
" Strip trailing whitespace (,sw)
function! StripWhitespace ()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    :%s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction
noremap <leader>sw :call StripWhitespace ()<CR>

" =========================================================================
" Auto
" =========================================================================

" Remove trailing whitespaces and ^M chars
" autocmd FileType c,cpp,java,php,javascript,python,twig,xml,yml,phtml,vimrc autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))


" re-source .vimrc on save so changes are effective immediately
augroup reload_vimrc
    autocmd!
    autocmd BufWritePost .vimrc source %
augroup END

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
augroup jump_lastposition
    autocmd!
    autocmd BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \ exe "normal g`\"" |
            \ endif
augroup END


" Load matchit.vim, but only if the user hasn't installed a newer version.
if !exists('g:loaded_matchit') && findfile('plugin/matchit.vim', &rtp) ==# ''
  runtime! macros/matchit.vim
endif


" =========================================================================
" File type specifics
" =========================================================================
" .inc, phpt, phps files as PHP
augroup php_filetypes
    autocmd!
    autocmd BufNewFile,BufRead *.inc set ft=php
"    autocmd BufNewFile,BufRead *.phtml set ft=php.html
augroup END

" add json syntax highlighting
au BufNewFile,BufRead *.json set ft=javascript

au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

" .md files are markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" =========================================================================
" Colour scheme
" =========================================================================
colorscheme rka
" colorscheme default
"
