" Vim color file
"
" Author: Rob Allen <rob@akrabat.com>
"
" Note: Based on Anthony Carapetis <anthony.carapetis@gmail.com>
"       'github' scheme which is based on github's syntax
"       highlighting theme
"       Used Brian Mock's darkspectrum as a starting point/template
"       Thanks to Ryan Heath for an easy list of some of the colours:
"       http://rpheath.com/posts/356-github-theme-for-syntax-gem

hi clear

set background=light
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
	syntax reset
    endif
endif
let g:colors_name="rka"

hi Normal       ctermfg=0 guifg=#000000 guibg=#FFFFFF

" {{{ Cursor
hi Cursor		ctermbg=238 ctermfg=255 guibg=#444444 guifg=#F8F8FF
hi CursorLine   guibg=#EEEEEE
hi CursorColumn	guibg=#E8E8EE
" }}}

" {{{ Diff
hi DiffAdd         guifg=#003300 guibg=#DDFFDD gui=none
hi DiffChange                    guibg=#ececec gui=none
hi DiffText        guifg=#000033 guibg=#DDDDFF gui=none
hi DiffDelete      guifg=#DDCCCC guibg=#FFDDDD gui=none
" }}}

" {{{ Folding / Line Numbering / Status Lines
hi Folded		guibg=#ECECEC guifg=#808080 gui=none
hi vimFold		guibg=#ECECEC guifg=#808080 gui=none
hi FoldColumn	guibg=#ECECEC guifg=#808080 gui=none

hi LineNr		guifg=#666666 guibg=#F0F0F0 gui=none
"hi NonText      guifg=#808080 guibg=#ECECEC
hi NonText		guifg=#808080 guibg=#FFFFFF
hi Folded		guifg=#808080 guibg=#ECECEC gui=none
hi FoldeColumn  guifg=#808080 guibg=#ECECEC gui=none

" hi VertSplit	guibg=#bbbbbb guifg=#bbbbbb gui=none
" hi StatusLine   guibg=#bbbbbb guifg=#404040 gui=none
" hi StatusLineNC guibg=#d4d4d4 guifg=#404040 gui=italic
" }}}

" {{{ Misc
hi ModeMsg		guifg=#990000
hi MoreMsg		guifg=#990000

hi Title		guifg=#ef5939
hi WarningMsg	guifg=#ef5939
hi SpecialKey   guifg=#177F80 gui=italic

hi MatchParen	ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline
hi Underlined	guifg=#000000 gui=underline
hi Directory	guifg=#990000
" }}}

" {{{ Search, Visual, etc
hi Visual		guifg=#FFFFFF guibg=#3465a4 gui=none
hi VisualNOS    guifg=#FFFFFF guibg=#204a87 gui=none
hi IncSearch	guibg=#cdcdfd guifg=#000000 gui=none
hi Search		guibg=#cdcdfd guifg=#000000 gui=none
" }}}

" {{{ Syntax groups
hi Ignore       ctermfg=240 guifg=#808080
hi Identifier   ctermfg=0 guifg=#000000
hi PreProc      ctermfg=232 guifg=#808080 gui=none
hi Comment      ctermfg=22 guifg=#3C802C
hi Constant     ctermfg=54 guifg=#0F20F6 gui=none
hi String       ctermfg=DarkRed guifg=#AA0000
hi Function     ctermfg=Black guifg=#990000 gui=none
hi Statement    ctermfg=0 guifg=#000000 gui=none
hi Type         guifg=#445588 gui=none
hi Number       ctermfg=Blue guifg=#0F20F6
hi Todo         ctermfg=15 ctermbg=8 guifg=#FFFFFF guibg=#990000 gui=none
hi Special      ctermfg=28  guifg=#159828 gui=none
hi rubySymbol   ctermfg=54 guifg=#960B73
hi Error        ctermfg=1 guibg=#f8f8ff guifg=#ff1100 gui=undercurl
hi Todo         ctermfg=1 guibg=#f8f8ff guifg=#ff1100 gui=underline
hi Label        ctermfg=0 guifg=#000000 gui=none
hi StorageClass ctermfg=0 guifg=#000000 gui=none
hi Structure    ctermfg=0 guifg=#000000 gui=none
hi TypeDef      ctermfg=0 guifg=#000000 gui=none
" }}}

" {{{ Completion menus
hi WildMenu     guifg=#7fbdff guibg=#425c78 gui=none

hi Pmenu        guibg=#808080 guifg=#ffffff gui=bold
hi PmenuSel     guibg=#cdcdfd guifg=#000000 gui=italic
hi PmenuSbar    guibg=#000000 guifg=#444444
hi PmenuThumb   guibg=#aaaaaa guifg=#aaaaaa
" }}}

" {{{ Spelling
hi spellBad     guisp=#fcaf3e
hi spellCap     guisp=#73d216
hi spellRare    guisp=#fcaf3e
hi spellLocal   guisp=#729fcf
" }}}

"{{{ HTML
hi htmlBold                guifg=#ff0000 gui=none
hi htmlBoldUnderline       guifg=#ff0000 gui=none
hi htmlBoldUnderlineItalic guifg=#ff0000 gui=none
hi htmlUnderline           guifg=#ff0000 gui=none
hi htmlUnderlineItalic     guifg=#ff0000 gui=none
hi htmlItalic              guifg=#ff0000 gui=none
hi htmlTitle               guifg=#ff0000 gui=none
hi htmlH1                  guifg=#ff0000 gui=none
hi htmlH2                  guifg=#ff0000 gui=none
hi htmlH3                  guifg=#ff0000 gui=none
hi htmlH4                  guifg=#ff0000 gui=none
hi htmlH5                  guifg=#ff0000 gui=none
hi htmlH6                  guifg=#ff0000 gui=none
"}}}

" {{{ Aliases
hi link cppSTL          Function
hi link cppSTLType      Type
hi link Character		Number
hi link htmlTag			htmlEndTag
"hi link htmlTagName     htmlTag
hi link htmlLink		Underlined
hi link pythonFunction	Identifier
hi link Question		Type
hi link CursorIM		Cursor
hi link VisualNOS		Visual
hi link xmlTag			Identifier
hi link xmlTagName		Identifier
hi link shDeref			Identifier
hi link shVariable		Function
hi link rubySharpBang	Special
hi link perlSharpBang	Special
hi link schemeFunc      Statement
"hi link shSpecialVariables Constant
"hi link bashSpecialVariables Constant
" }}}

" {{{ Tabs (non-gui0
hi TabLine		guifg=#404040 guibg=#dddddd gui=none
hi TabLineFill	guifg=#404040 guibg=#dddddd gui=none
hi TabLineSel	guifg=#404040 gui=bold
" }}}

" {{{ RKA extra
"hi ExtraWhitespace ctermbg=95 guibg=#FFE4FF

" }}}

" vim: sw=4 ts=4 foldmethod=marker
