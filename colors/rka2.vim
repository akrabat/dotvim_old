" Vim color file
"
" Author: Rob Allen <rob@akrabat.com>
"
" Note: Based on Anthony Carapetis <anthony.carapetis@gmail.com>
"       'github' scheme which is based on github's syntax
"       highlighting theme
"       Used Brian Mock's darkspectrum as a starting point/template
"       Thanks to Ryan Heath for an easy list of some of the colours:
"       http://rpheath.com/posts/356-github-theme-for-syntax-gem

hi clear

set background=light
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
	syntax reset
    endif
endif
let g:colors_name="rka"

hi Normal       guifg=#000000 guibg=#FFFFFF

" {{{ Cursor
hi Cursor		guibg=#444454 guifg=#F8F8FF   ctermfg=black ctermbg=white
hi CursorLine   guibg=#EEEEEE
hi CursorColumn	guibg=#E8E8EE
" }}}

" {{{ Diff
hi DiffAdd         guifg=#003300 guibg=#DDFFDD gui=none
hi DiffChange                    guibg=#ececec gui=none
hi DiffText        guifg=#000033 guibg=#DDDDFF gui=none
hi DiffDelete      guifg=#DDCCCC guibg=#FFDDDD gui=none
" }}}

" {{{ Folding / Line Numbering / Status Lines
hi Folded		guibg=#ECECEC guifg=#808080 gui=none
hi vimFold		guibg=#ECECEC guifg=#808080 gui=none
hi FoldColumn	guibg=#ECECEC guifg=#808080 gui=none

hi LineNr		guifg=#666666 guibg=#F0F0F0 gui=none
"hi NonText      guifg=#808080 guibg=#ECECEC
hi NonText		guifg=#808080 guibg=#FFFFFF
hi Folded		guifg=#808080 guibg=#ECECEC gui=none
hi FoldeColumn  guifg=#808080 guibg=#ECECEC gui=none

hi StatusLine   guifg=DarkGreen ctermfg=8 guibg=White ctermbg=15
hi StatusLineNC guifg=Black ctermfg=0 guibg=White ctermbg=15

" }}}

" {{{ Misc
hi ModeMsg		guifg=#990000
hi MoreMsg		guifg=#990000

hi Title		guifg=#ef5939
hi WarningMsg	guifg=#ef5939
hi SpecialKey   guifg=#177F80 gui=italic

hi MatchParen	ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline
hi Underlined	guifg=#000000 gui=underline
hi Directory	guifg=#990000
" }}}

" {{{ Search, Visual, etc
hi Visual		guifg=#FFFFFF guibg=#3465a4 gui=none
hi VisualNOS    guifg=#FFFFFF guibg=#204a87 gui=none
hi IncSearch	guibg=#cdcdfd guifg=#000000 gui=none
hi Search		guibg=#cdcdfd guifg=#000000 gui=none
" }}}

" {{{ Syntax groups
hi Ignore		guifg=#808080
hi Identifier	guifg=#000000
hi PreProc		guifg=#606060 gui=none
hi Comment		guifg=#3C802C gui=italic
hi Constant		guifg=#177F80 gui=none
hi String		guifg=#D81745
hi Function		guifg=#990000 gui=none
hi Statement	guifg=#0829AF gui=none
hi Type			guifg=#445588 gui=none
hi Number		guifg=#1C9898
hi Todo			guifg=#FFFFFF guibg=#990000 gui=none
hi Special		guifg=#159828 gui=none
hi rubySymbol   guifg=#960B73
hi Error        guibg=#f8f8ff guifg=#ff1100 gui=undercurl
hi Todo         guibg=#f8f8ff guifg=#ff1100 gui=underline
hi Label        guifg=#000000 gui=none
hi StorageClass guifg=#000000 gui=none
hi Structure    guifg=#ff0000 gui=none
hi TypeDef      guifg=#000000 gui=none
" }}}

" {{{ Completion menus
hi WildMenu     guifg=#7fbdff guibg=#425c78 gui=none

hi Pmenu        guibg=#808080 guifg=#ffffff gui=bold
hi PmenuSel     guibg=#cdcdfd guifg=#000000 gui=italic
hi PmenuSbar    guibg=#000000 guifg=#444444
hi PmenuThumb   guibg=#aaaaaa guifg=#aaaaaa
" }}}

" {{{ Spelling
hi spellBad     guisp=#fcaf3e
hi spellCap     guisp=#73d216
hi spellRare    guisp=#fcaf3e
hi spellLocal   guisp=#729fcf
" }}}

"{{{ HTML
hi htmlBold                gui=none
hi htmlBoldUnderline       gui=none
hi htmlBoldUnderlineItalic gui=none
hi htmlUnderline           gui=none
hi htmlUnderlineItalic     gui=none
hi htmlItalic              gui=none
hi htmlTitle               gui=none
hi htmlH1                  gui=none
hi htmlH2                  gui=none
hi htmlH3                  gui=none
hi htmlH4                  gui=none
hi htmlH5                  gui=none
hi htmlH6                  gui=none
"}}}

" {{{ Aliases
hi link cppSTL          Function
hi link cppSTLType      Type
hi link Character		Number
hi link htmlTag			htmlEndTag
"hi link htmlTagName     htmlTag
hi link htmlLink		Underlined
hi link pythonFunction	Identifier
hi link Question		Type
hi link CursorIM		Cursor
hi link VisualNOS		Visual
hi link xmlTag			Identifier
hi link xmlTagName		Identifier
hi link shDeref			Identifier
hi link shVariable		Function
hi link rubySharpBang	Special
hi link perlSharpBang	Special
hi link schemeFunc      Statement
"hi link shSpecialVariables Constant
"hi link bashSpecialVariables Constant
" }}}

" {{{ Tabs (non-gui0
hi TabLine		guifg=#404040 guibg=#dddddd gui=none
hi TabLineFill	guifg=#404040 guibg=#dddddd gui=none
hi TabLineSel	guifg=#404040 gui=bold
" }}}

" {{{ Perl
hi perlStringStartEnd guifg=#000000
" }}}

" {{{ RKA extra
"hi ExtraWhitespace ctermbg=95 guibg=#FFE4FF

" }}}

" vim: sw=4 ts=4 foldmethod=marker
